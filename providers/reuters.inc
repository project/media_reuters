<?php

/**
 *	@file
 *		This include processes Reuters videos for use by the emfield.module
 */

define('MEDIA_REUTERS_URL', 'http://www.reuters.com/');
define('MEDIA_REUTERS_URL_PLAYER', 'http://www.reuters.com/resources_v2/flash/video_embed.swf');
define('MEDIA_REUTERS_METADATA_ENDPOINT', 'http://www.reuters.com/embeddedVideo');

define('MEDIA_REUTERS_DATA_VERSION', 1);

/**
 * hook emvideo_PROVIDER_info
 * this returns information relevant to a specific 3rd party video provider
 * @return
 *   an array of strings requested by various admin and other forms
 *   'name' => the translated name of the provider
 *   'url' => the url to the main page for the provider
 *   'settings_description' => a description of the provider that will be posted in the admin settings form
 *   'supported_features' => an array of rows describing the state of certain supported features by the provider.
 *      These will be rendered in a table, with the columns being 'Feature', 'Supported', 'Notes'.
 */
function emvideo_reuters_info() {
  $features = array(
    array(t('Autoplay'), t('No'), ''),
    array(t('RSS Attachment'), t('Yes'), ''),
    array(t('Thumbnails'), t('Yes'), t('')),
    array(
      t('Full screen mode'),
      t('Yes'),
      t('You may customize the player to enable or disable full screen playback. Full screen mode is enabled by default.')),
  );
  return array(
    'provider' => 'reuters',
    'name' => t('Reuters'),
    'url' => MEDIA_REUTERS_URL,
    'settings_description' => t('These settings specifically affect videos displayed from !provider.', 
  		array('!provider' => l('reuters.com', MEDIA_REUTERS_URL))),
    'supported_features' => $features,
  );
}

/**
 *  hook emvideo_PROVIDER_settings
 */
function emvideo_reuters_settings() {
  $form['reuters']['player_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Embedded video player options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  // This is an option to set the video to full screen.
  $form['reuters']['player_options']['emvideo_reuters_full_screen'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow fullscreen'),
    '#default_value' => variable_get('emvideo_reuters_full_screen', 1),
    '#description' => t('Allow users to view video using the entire computer screen.'),
  );

  return $form;
}

/**
 * hook emvideo_PROVIDER_extract
 * this is called to extract the video code from a pasted URL or embed code.
 * @param $code
 *   an optional string with the pasted URL or embed code
 * @return
 *   either an array of regex expressions to be tested, or a string with the video code to be used
 *   if the hook tests the code itself, it should return either the string of the video code (if matched), or an empty array.
 *   otherwise, the calling function will handle testing the embed code against each regex string in the returned array.
 */
function emvideo_reuters_extract($embed = '') {
	
// http://www.reuters.com/video/2011/06/13/airbus-view-of-flying-in-the-future?videoId=215401287		
/*
	<object type='application/x-shockwave-flash' data='http://www.reuters.com/resources_v2/flash/video_embed.swf?videoId=215401287' id='rcomVideo_215401287' width='460' height='259'>
	  <param name='movie' value='http://www.reuters.com/resources_v2/flash/video_embed.swf?videoId=215401287'>
	  <param name='allowFullScreen' value='true'></param> <param name='allowScriptAccess' value='always'>
	  <param name='wmode' value='transparent'>
	  <embed src='http://www.reuters.com/resources_v2/flash/video_embed.swf?videoId=215401287' type='application/x-shockwave-flash' allowfullscreen='true' allowScriptAccess='always' width='460' height='259' wmode='transparent'></embed>
	</object>
*/
	return array(
		'@videoId\=([0-9]+)@i'
	);
}

/**
 * hook emfield_PROVIDER_data
 *
 * provides an array to be serialised and made available with $item elsewhere
 */
function emvideo_reuters_data($field, $item) {
  // Initialize the data array.
  $data = array();
  $data['emvideo_reuters_version'] = MEDIA_REUTERS_DATA_VERSION;
  
  // Yahoo!
  //http://www.reuters.com/embeddedVideo?videoId=215401287
  
  $response = emfield_request_xml('reuters', MEDIA_REUTERS_METADATA_ENDPOINT, array('videoId' => $item['value']));
  
  if (isset($response['VIDEO'])) {
  	$data['thumbnail']	= $response['VIDEO']['VIDEOTHUMB'][0];
  	$data['duration']		= $response['VIDEO']['VIDEOLENGTH'][0];
  }
  
  media_reuters_data($item, $data);

  return $data;
}

/**
 * hook emvideo_PROVIDER_embedded_link($video_code)
 * returns a link to view the video at the provider's site.
 *  @param $video_code
 *    The string containing the video to watch.
 *  @return
 *    A string containing the URL to view the video @ the original provider site.
 */
function emvideo_reuters_embedded_link($video_code, $data) {	
	return isset($data['link']) ? $data['link'] : MEDIA_REUTERS_URL;	
}

/**
 *  hook emvideo_PROVIDER_rss
 *  This attaches a file to an RSS feed.
 *  
 *  @TODO Get the video URL from http://www.reuters.com/embeddedVideo
 */
function emvideo_reuters_rss($item, $teaser = NULL) {

	if ($item['value']) {
    $file['filepath'] = MEDIA_REUTERS_URL_PLAYER .'?videoId='. $item['value'];
    $file['filemime'] = 'application/x-shockwave-flash';	
    
    return $file;
  }
}

/**
 * hook emvideo_PROVIDER_thumbnail
 * Returns the external url for a thumbnail of a specific video.
 *  @param $field
 *    The field of the requesting node.
 *  @param $item
 *    The actual content of the field from the requesting node.
 *  @return
 *    A URL pointing to the thumbnail.
 */
function emvideo_reuters_thumbnail($field, $item, $formatter, $node, $width, $height) {
	// http://s1.reutersmedia.net/resources/r/?d=20110613&i=215401287&w=420&r=21338292&t=2
	
  if (isset($item['data']['thumbnail']))  {
  	preg_replace('@(w=[0-9]+)@', "w=$width", $item['data']['thumbnail']);
  	
    return $item['data']['thumbnail'];
  }
  
  // extreme fallback.
  return 'http://thomsonreuters.com/content/media/images/brandguide/reuters/rtr_ahz_rgb_pos.png';
}

/**
 * hook emvideo_PROVIDER_duration
 * Return movie duration
 */
function emvideo_reuters_duration($item) {
	if (isset($item['data']['duration'])) {
		return $item['data']['duration'];
	}
	
	return 0;
}

/**
 *  hook emvideo_PROVIDER_video
 *  This actually displays the full/normal-sized video we want, usually on the
 *  default page view.
 */
function emvideo_reuters_video($embed, $width, $height, $field, $item, $node, $autoplay) {	
	$tn_options = array(
  	'width'			=> $width,
    'height' 		=> $height,
    'link_url'	=> isset($item['data']['link']) ? $item['data']['link'] : MEDIA_REUTERS_URL,
  );
  
	$thumbnail = theme('emvideo_video_thumbnail', NULL, $item, 'emvideo_thumbnail', $node, FALSE, $tn_options);
  
	return theme('emvideo_reuters_flash', $item, $width, $height, $thumbnail);
}

/**
 * The embedded code
 */
function theme_emvideo_reuters_flash($item, $width, $height, $thumbnail) {
  $output = '';
  
  if ($item['embed']) {    
    $movie			= MEDIA_REUTERS_URL_PLAYER .'?videoId='. $item['value'];
    $fullscreen = variable_get('emvideo_reuters_full_screen', 1) ? 'true' : 'false'; 

    $output = <<<EOD
<object type="application/x-shockwave-flash" data="$movie" width="$width" height="$height">
  <param name="movie" value="$movie" />
	<param name="allowFullScreen" value="$fullscreen" />
	<param name="allowscriptaccess" value="always" />
	<param name="wmode" value="transparent" />	
	$thumbnail
</object>
EOD;
  }

  return $output;
}

/**
 * Implementation of hook_emfield_subtheme().
 * This returns any theme functions defined by this provider.
 */
function emvideo_reuters_emfield_subtheme() {
  $themes = array(
    'emvideo_reuters_flash'  => array(
      'arguments' => array(
      	'item' => NULL, 'width' => NULL, 'height' => NULL, $thumbnail => NULL
  		),
      'file' => 'providers/reuters.inc',
      'path' => drupal_get_path('module', 'media_reuters'),
    )
  );
  return $themes;
}